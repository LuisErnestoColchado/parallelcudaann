
#include <iostream>
#include "Neuron.cpp"
#include "Connection.cpp"
#include "DB.cpp"
#include <omp.h>

using namespace std;

/*
 * Variable ArrayStudent
 * Arreglo bidimencional de los parametros de entrada de los 80 estudiantes.
 * Cada estudiantes tiene la siguiente información:
 *
 * cod: Codigo del estudiante
 * name: Nombre del estudiante
 * Age: Edad del estudiante
 * index1: indicador numero 1;
 * index2: indicador numero 2;
 * index3: indicador numero 3;
 * index4: indicador numero 4;
 * index5: indicador numero 5;
 * index6: indicador numero 6;
 * index7: indicador numero 7;
 * index8: indicador numero 8;
 * index9: indicador numero 9;
 * index10: indicador numero 10;
 * index11: indicador numero 11;
 * result: resultado real del estudiante
 */
Student* ArrayStudent;
const char* sql = "QueryText";

/*
 * Contador
 */
int count=0;

__global__ void addParalell() {

}

/*
 * Programa Principal
 */
int main(int argc, char* argv[])
{
	int thread_count = strtol(argv[1],  NULL, 10);
	string textinsert="";
	/*
	 * error cometido para el alumno n
	 * y acumulador
	 * */
	double error = 0;
	double ErrorSum=0;

	/*
	 * Error total en el entrenamiento
	 * */
	double totalErrorTraining = 0;
	double totalErrorValidation = 0;

	/*
	 * Contadores
	 */
	int n = 0;

	/*
	 * Numero de capas de contexto
	 */
	int NumberNeurons = 0;

	/*
	 * Razon de aprendizaje para la etapa de validación
	 * */
	double LearningRatio = 0.001;

	/*
	 * Varible real, para almacenar valores aleatorios
	 */
	double random;

	/*
	 * Indicador del tipo de función. Las Funciones validas para backpropagation son:
	 * 1. Función Sigmoidal
	 * 2. Función Hiperbolica
	 */
	int TypeFunction;
	string function;

	/*
	 * puntero de puntero a objetos de tipo connection (Connection.cpp)
	 */
	Connection ** connection;

	Connection **dev_conection;
	/*
	 * puntero a objetos de tipo neuron (Neuron.cpp)
	 */
	Neuron* neuron;

	Neuron * dev_neuron;
	/*
	 * Declaramos la variable tiempo
	 */
	int Time;
	int CountTime = 1;

	/*
	 * Puntero de vector bidimensional de los valores de errores de cada neurna
	 */
	double** NeuronError;

	double MUTime;
	double ompTime;

	ArrayStudent = new Student[961];

	/*
	 * Leemos los parametros de la red de tipo Perceptron Multicapa y los parametros necesarios para
	 * el aprendizaje de tipo de retropropagacion para atras (backpropagation)
	 */
	cout<<"Ingrese el tiempo \n";
	cin>>Time;
	cout<<"\nTIPOS DE FUNCIÓN DE ACTIVACIÓN PARA BACKPROPAGATION:\n\n"
			"Opción 1. Función Sigmoidal"
			"\nOpción 2. Función Hiperbolica\n"
			"\nSeleccione un numero de la función que desea utilizar:\n";
	cin>>TypeFunction;
	if(TypeFunction == 1)function="sigmoide";
	else function = "hyperbolic";
	cout<<"Ingrese el numero de neuronas de contexto para la red recurrente: \n";
	cin>>NumberNeurons;
	NumberNeurons = NumberNeurons + 11;//aumentamos las 11 nueronas de entrada

	//consturimos la arquitectura
	neuron = new Neuron[NumberNeurons];
	double cache[NumberNeurons];
	connection = new Connection *[NumberNeurons];
	for(int i = 0;i<NumberNeurons;i++)
	{
		neuron[i].SetTypeFunction(TypeFunction);
		//las entrdas no son recurrentes
		connection[i] = new Connection[NumberNeurons];
		for(int j = 11;j<NumberNeurons;j++)
		{
			random = ((double)rand()/(RAND_MAX));
			connection[i][j].SetWeightValue(random);
		}
	}

	double RealTimeDerived[NumberNeurons];

	/*
	 * Salida del sistema dinamico
	 */
	double OutDinamicSystem[NumberNeurons][NumberNeurons];

	double ** OutDinamicSystem;
	/*
	 * Acumulador
	 */
	double SumOutDinamicSystem[NumberNeurons][NumberNeurons];
	
	double ** dev_SumOutDinamicSystem;
	/*
	 * conexion a base de datos
	 */
	DB database;
	bool resultDB;
	resultDB = database.ConexionDB();
	if(resultDB == true)cout<<"Open Database"<<endl;
	else cout<<"Error opening Database"<<endl;

	/*
	 * conseguir datos de los estudiantes
	 */
	database.SetCount(0);
	resultDB = database.GetStudents();
	if(resultDB == true){
		cout<<"Student data obtained"<<endl;
		ArrayStudent = database.GetStudentsVector();
	}
	else cout<<"Error getting student data"<<endl;

	double MSE;

	textinsert="";

	for(int i = 0;i<NumberNeurons;i++)
	{
		for(int j = 11;j<NumberNeurons;j++)
		{
			random = ((double)rand()/(RAND_MAX));
			connection[i][j].SetWeightValue(random);
		}
	}

	/*
	 * inicializamos el valor de las salidas de las neuronas
	 */
	for(int i = 11;i<NumberNeurons;i++)
    {
	    neuron[i].SetResultActivationFunction(0);
    }

	/*
	 * Inicializamos la salida dinamica del sistema
	 */
    for(int i =0;i<NumberNeurons;i++)
	{
		for(int j =11;j<NumberNeurons;j++){
		    OutDinamicSystem[i][j] = 0;
			SumOutDinamicSystem[i][j] = 0;
		}
	}

	totalErrorTraining = 0;
	MSE=0;
	ErrorSum = 0;
	textinsert="";
    n=1;

    //Iniciamos el contador de tiempo de ejecucion
    double trainingTime = omp_get_wtime();

	/*
	 * Recorremos los tiempos establecidos
	 */
	for(CountTime = 1;CountTime <= Time;CountTime++)
	{
	    /*
		 * Conseguimos el patron de entrada n
		 */
		neuron[0].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex1()));
		neuron[1].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex2()));
		neuron[2].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex3()));
		neuron[3].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex4()));
		neuron[4].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex5()));
		neuron[5].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex6()));
		neuron[6].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex7()));
		neuron[7].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex8()));
		neuron[8].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex9()));
		neuron[9].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex10()));
		neuron[10].SetResultActivationFunction(stold(ArrayStudent[n].GetIndex11()));

		for(int i = 11;i<NumberNeurons;i++)
		{
		    MUTime =0;
			for(int j = 0;j<NumberNeurons;j++){
			    MUTime = MUTime + (connection[j][i].GetWeightValue() * neuron[j].GetResultActivationFunction());
			}
			neuron[i].SetMU(MUTime);
			neuron[i].StartActivateFunction();

			if(i==NumberNeurons - 1)
			{
			    error = stold(ArrayStudent[n].GetResult()) - neuron[i].GetResultActivationFunction();
			}
		}

#pragma omp parallel for num_threads(thread_count) collapse(3)
		/*
		 * hallar sumat1
		 */
        for(int i = 11;i < NumberNeurons;i++)
		{
		    for(int j = 0;j < NumberNeurons;j++)
			{
				for(int k=11;k<NumberNeurons;k++)
				{
				    SumOutDinamicSystem[j][i] = SumOutDinamicSystem[j][i] + ((connection[k][i].GetWeightValue() * OutDinamicSystem[j][i]) + neuron[j].GetResultActivationFunction());
				}
			}
		}

#pragma omp parallel for num_threads(thread_count) collapse(2)
	    /*
		 * hallar salidas dinamicas del sistema para el tiempo actual
		 */
		for(int i = 11;i < NumberNeurons;i++)
		{
		    for(int j = 0;j < NumberNeurons;j++)
			{
				/*
				 * Las salidas dinamicas influyen en la actualizacion de los pesos de las conexiones
				 */
				OutDinamicSystem[j][i] = neuron[i].StartDerivateActivationFunction() * SumOutDinamicSystem[j][i];
				connection[j][i].UpdateWeightValue(LearningRatio,error,OutDinamicSystem[j][i]);
			}
		}

		error = 0.5 * pow(error,2);
		ErrorSum = ErrorSum + error;

		totalErrorTraining = ErrorSum/(n+1);
		//cout << " tiempo " << CountTime << " error " << totalErrorTraining << endl;

		ErrorSum = 0;

		if(n==800)
		    n = 1;
		else
		    n++;

		CountTime++;
    }
	trainingTime = omp_get_wtime() - trainingTime;
    cout<<"Error total " << totalErrorTraining << endl;
    cout << "El tiempo de entrenamiento fue: " << ompTime << endl;

    /*
     * Validation
	 */
    int CountSubset = 801;

    while(CountSubset <= 960)
	{
	    //inicializamos entradas
		neuron[0].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex1()));
		neuron[1].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex2()));
		neuron[2].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex3()));
		neuron[3].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex4()));
		neuron[4].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex5()));
		neuron[5].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex6()));
		neuron[6].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex7()));
		neuron[7].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex8()));
		neuron[8].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex9()));
		neuron[9].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex10()));
		neuron[10].SetResultActivationFunction(stold(ArrayStudent[CountSubset].GetIndex11()));

		for(int i = 11;i<NumberNeurons;i++){
		    MUTime =0;
			for(int j = 0;j<NumberNeurons;j++){
				MUTime = MUTime + (connection[j][i].GetWeightValue() * neuron[j].GetResultActivationFunction());
			}
			neuron[i].SetMU(MUTime);
			cache[i] = neuron[i].GetResultActivationFunction();
			neuron[i].StartActivateFunction();
		}
		cout<< "student " << CountSubset << " salida "<<neuron[NumberNeurons - 1].GetResultActivationFunction() << endl;

		MSE = MSE + pow((stold(ArrayStudent[CountSubset].GetResult()) - neuron[NumberNeurons - 1].GetResultActivationFunction()),2);
		CountSubset++;

		for(int i = 11;i<NumberNeurons;i++){
			neuron[i].SetResultActivationFunction(cache[i]);
		}

    }

    textinsert="";
	totalErrorValidation = (1/160.0) * (0.5*MSE);
	textinsert = textinsert + "insert into ResultRealTime values(" + to_string(Time) + ", " + to_string(totalErrorTraining) + ", " +  to_string(totalErrorValidation) + "," + to_string(trainingTime) + " , " + to_string(thread_count) + " ,'" + function + "');";
	sql = textinsert.c_str();

	resultDB = database.InsertResult(sql);
	if(resultDB == true)cout<<"Records created successfully"<<endl;
	else cout<<"Error while registering the date"<<endl;
	cout<<"Error de validación " << totalErrorValidation << endl;

	delete[] neuron;
	delete[] connection;
	delete[] NeuronError;
}
