#include <iostream>

class Connection{

	private:

		//int Layer;

		double TotalError;


	public:

		Connection();

		double WeightValue; //Inicalizamos el peso de la conexion en un aletorio entre 0 y 1
		
		Connection(int,double);

		void SetWeightValue(double);

		double GetWeightValue();

		void SetTotalError(double);

		double GetTotalError();


		double UpdateWeightValue(double,double,double);

};



Connection::Connection(int layer,double weightvalue){
	//Layer = layer;
	WeightValue = weightvalue;
}

Connection::Connection(){}

void Connection::SetWeightValue(double weightvalue){
	WeightValue = weightvalue;
}

double Connection::GetWeightValue(){
	return WeightValue;
}

void Connection::SetTotalError(double totalerror){
	TotalError = totalerror;
}

double Connection::GetTotalError(){
	return TotalError;
}

double Connection::UpdateWeightValue(double reasonlearning,double resultactivation,double error)
{
	WeightValue = (WeightValue  + (reasonlearning *  error * resultactivation));
	return WeightValue;
}
