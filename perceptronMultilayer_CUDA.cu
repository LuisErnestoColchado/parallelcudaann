#include <iostream>
#include <cmath>
#include <sqlite3.h>
#include <string>
#include <cstring>
#include <vector>
#include <sstream>
#include <stdlib.h>
#include "Neuron.cpp"
#include "Connection.cpp"
#include "DB.cpp"
#include <cuda_runtime.h>
#include <cuda.h>
#include <stdio.h>
#include <math.h>
#include "common/book.h"
using namespace std;

/*
 * Variable ArrayStudent
 * Arreglo bidimencional de los parametros de entrada de los 283 estudiantes.
 * Cada estudiantes tiene la siguiente información:
 *
 * cod: Codigo del estudiante
 * name: Nombre del estudiante
 * Age: Edad del estudiante
 * index1: indicador numero 1;
 * index2: indicador numero 2;
 * index3: indicador numero 3;
 * index4: indicador numero 4;
 * index5: indicador numero 5;
 * index6: indicador numero 6;
 * index7: indicador numero 7;
 * index8: indicador numero 8;
 * index9: indicador numero 9;
 * index10: indicador numero 10;
 * index11: indicador numero 11;
 * result: resultado real del estudiante
 */
Student *ArrayStudent;
const char *sql = "QueryText";
#define     e      (2.71828) //definimos el valor del numero e

/*
 * Contador
 */
int count = 0;

__global__ void readInput(Neuron * pNeuron,int * pN,Student * Students){
	int tid = threadIdx.x;
	if(tid == 0)
		pNeuron[tid].ResultActivationFunction = Students[pN[0]].index1;
	if(tid == 1)
		pNeuron[tid].ResultActivationFunction = Students[pN[0]].index2;
	if(tid == 2)
		pNeuron[tid].ResultActivationFunction = Students[pN[0]].index3;
	if(tid == 3)
		pNeuron[tid].ResultActivationFunction = Students[pN[0]].index4;		
	if(tid == 4)
		pNeuron[tid].ResultActivationFunction = Students[pN[0]].index5;
	if(tid == 5)
		pNeuron[tid].ResultActivationFunction = Students[pN[0]].index6;
	if(tid == 6)
		pNeuron[tid].ResultActivationFunction = Students[pN[0]].index7;
	if(tid == 7)
		pNeuron[tid].ResultActivationFunction = Students[pN[0]].index8;
	if(tid == 8)
		pNeuron[tid].ResultActivationFunction = Students[pN[0]].index9;
	if(tid == 9)
		pNeuron[tid].ResultActivationFunction = Students[pN[0]].index10;		
	if(tid == 10)
		pNeuron[tid].ResultActivationFunction = Students[pN[0]].index11;
	//printf(" value %lf \n",pNeuron[tid].ResultActivationFunction);
}
/*
__global__ void updateAuxMu(double * auxmu_cuda,int * countNeuron){
	for(int i = 0;i< countNeuron;i++){
		auxmu_cuda[i] = 0;
	}
}*/

__global__ void updateN(int * dev_n){
	dev_n[0] += 1;
}

__global__ void _updateN(int * dev_n){
	dev_n[0] = 1;
}

__global__ void _updateError(double * pError){
	pError[0] = 0;
}

__global__ void forwardItoC(Neuron * pNeuron,Connection * pConnection,int  * countNeuron,double * auxmu_cuda){
	int tid = threadIdx.x +blockIdx.x * blockDim.x;
	/*for(int i = 0;i < 11; i++){
		printf(" value %lf",pNeuron[i].ResultActivationFunction); 		
	}*/	
	auxmu_cuda[tid] = 0;
	if(tid > 10 && tid < countNeuron[0]-1){
		for(int l = 0;l<11;l++){
			int indexNeuron = (tid-11)*11+l;
			auxmu_cuda[tid] += pNeuron[l].ResultActivationFunction * pConnection[indexNeuron].WeightValue;  
		}
		pNeuron[tid].MU = auxmu_cuda[tid] + pNeuron[tid].Bias;
		pNeuron[tid].ResultActivationFunction = (1.0 / (1.0 + pow(e,-pNeuron[tid].MU)));
		//printf(" %d :  %lf ",tid,pNeuron[tid].ResultActivationFunction);
		auxmu_cuda[tid] =0;
	}
}

__global__ void forwardCtoO(Neuron * pNeuron,Connection * pConnection,int  * countNeuron,double * auxmu_cuda,int * count){
	int tid = threadIdx.x +blockIdx.x * blockDim.x;
	int indexsum = threadIdx.x;
	auxmu_cuda[tid] = 0;
	__shared__ float sum[1024];
	if(tid > 10 && tid < countNeuron[0]-1){
		int indexConnection = (countNeuron[0]-12)*11+(tid-11);
		
		//printf("idex connect %d\n",indexConnection);
		auxmu_cuda[tid] = pNeuron[tid].ResultActivationFunction * pConnection[indexConnection].WeightValue; 
		//printf("pares %lf : %lf \n",pNeuron[tid].ResultActivationFunction,pConnection[indexConnection].WeightValue);
		sum[indexsum] = auxmu_cuda[tid];
		//count[0]++; 
	}
	__syncthreads();

	int i=blockDim.x/2;
	while(i!=0){
		if(indexsum < i)
			sum[indexsum] += sum[indexsum + i];
		__syncthreads();
		i/=2;
	}
	
	if(tid == countNeuron[0]-1){
		auxmu_cuda[countNeuron[0]-1] = sum[0];
		pNeuron[countNeuron[0]-1].MU = auxmu_cuda[countNeuron[0]-1] + pNeuron[countNeuron[0]-1].Bias;
		pNeuron[countNeuron[0]-1].ResultActivationFunction = (1.0 / (1.0 + pow(e,-pNeuron[countNeuron[0]-1].MU)));
		auxmu_cuda[countNeuron[0]-1] = 0;
		//printf("salida %lf\n",pNeuron[countNeuron[0]-1].ResultActivationFunction);
		//count[0]=0;
	}
}

__global__ void getErrorOutput(Neuron * pNeuron,double * pNeuronError,Connection * pConnection,int * pN,int * countNeuron,Student * students){
	pNeuronError[countNeuron[0]-1] = students[pN[0]].result - pNeuron[countNeuron[0]-1].ResultActivationFunction;
}

__global__ void backpropagation(Neuron * pNeuron,double * pNeuronError,Connection * pConnection,int * pN,int * countNeuron,Student * students){
	int tid = threadIdx.x +blockIdx.x * blockDim.x;
	if(tid > 10 && tid < countNeuron[0]-1){
		pNeuronError[tid] = ((1.0 / (1.0 + pow(e,-pNeuron[tid].MU))) * (1 - (1.0 / (1.0 + pow(e,-pNeuron[tid].MU))))) * (pConnection[(countNeuron[0]-12)*11+(tid-11)].WeightValue * pNeuron[tid].ResultActivationFunction);
		pNeuron[tid].ResultDerivateActivationFunction = (1.0 / (1.0 + pow(e,-pNeuron[tid].MU))) * (1 - (1.0 / (1.0 + pow(e,-pNeuron[tid].MU))));
	}
}

__global__ void updateWeight_bias_input(Neuron * pNeuron,double * pNeuronError,Connection * pConnection,int * countNeuron,double * pLearningRatio){
	int tid = threadIdx.x +blockIdx.x * blockDim.x;
	if(tid > 10 && tid < countNeuron[0] -1){

		pNeuron[tid].Bias += pLearningRatio[0] * pNeuronError[tid];
		for(int j = 0;j<11;j++){
			pConnection[((tid-11)*11)+j].WeightValue += pLearningRatio[0] * pNeuronError[tid] * pNeuron[j].ResultActivationFunction; 
		}
	}
}

__global__ void updateWeight_bias_oculta(Neuron * pNeuron,double * pNeuronError,Connection * pConnection,int * countNeuron,double * pLearningRatio){
	int tid = threadIdx.x +blockIdx.x * blockDim.x;
	if(tid == countNeuron[0]-1)
		pNeuron[tid].Bias += pLearningRatio[0] * pNeuronError[countNeuron[0]-1];	
	if(tid > 10 && tid < countNeuron[0]-1){
		//printf("peso prev %lf\n",pConnection[((countNeuron[0]-12)*11)+count].WeightValue);
		pConnection[((countNeuron[0]-12)*11)+(tid-11)].WeightValue += pLearningRatio[0] * pNeuronError[countNeuron[0]-1] * pNeuron[tid].ResultActivationFunction; 
		//printf("peso now %lf\n",pConnection[((countNeuron[0]-12)*11)+count].WeightValue);
	}
}

__global__ void errorSum(Neuron * pNeuron,double * pError,Student * students,int * pN,int * countNeuron){
	pError[0] += pow((students[pN[0]].result - pNeuron[countNeuron[0]-1].ResultActivationFunction),2.0);
}

__global__ void updateLearningRatio(double * pLearningRatio){
	pLearningRatio[0] += 0.00001;
}

__global__ void startTraining(){}

/*
 * Programa Principal
 */
int main(int argc, char *argv[])
{
	int thread_count = strtol(argv[1], NULL, 10);
	/*
	 * error cometido para el alu
	 * mno n
	 * y acumulador
	 * */
	double error = 0;
	double ErrorSum = 0;

	/*
	 * Error total en el entrenamiento
	 * */
	double trainingError = 0;

	/*
	 * Contador (indicador del patron actual)
	 */
	int n = 0;

	/*
	 * Numero de capas
	 */
	int NumberLayer = 0;

	/*
	 * Razon de aprendizaje para la etapa de validación
	 * */
	double LearningRatio = 0.00001;

	/*
	 * Numero de ciclos de aprendizaje para cada estudiante n
	 * Contador de ciclos de aprendizaje
	 * */
	double LearningCycle;
	double CountLearningCycle = 1;

	stringstream NumberString;

	/*
	 * Varible real, para almacenar valores aleatorios
	 * */
	double random;

	/*
	 * Indicador del tipo de función. Las Funciones validas para backpropagation son:
	 * 1. Función Sigmoidal
	 * 2. Función Hiperbolica
	 * */
	int TypeFunction;
	string function;

	/*
	 * Puntero de vector tridimensional de objetos de tipo connection (Connection.cpp)
	 */
	Connection ***connection;

	/*
	 * Puntero de vector bidimensional de objetos de tipo neuron (Neuron.cpp)
	 */
	Neuron ** neuron;

	/*
	 * Puntero de vector bidimensional de los valores de errores de cada neurona en la capa oculta
	 */
	double **NeuronError;

	/*Establecemos el numero de patrones*/
	ArrayStudent = new Student[961];

	/*
	 * Leemos los parametros de la red de tipo Perceptron Multicapa y los parametros necesarios para
	 * el aprendizaje de tipo de retropropagacion para atras (backpropagation)
	 */
	cout << "Ingrese el numero de ciclos de aprendizaje \n";
	cin >> LearningCycle;
	cout << "\nTIPOS DE FUNCIÓN DE ACTIVACIÓN PARA BACKPROPAGATION:\n\n"
			"Opción 1. Función Sigmoidal"
			"\nOpción 2. Función Hiperbolica\n"
			"\nSeleccione un numero de la función que desea utilizar:\n";
	cin >> TypeFunction;
	if (TypeFunction == 1)
		function = "sigmoide";
	else
		function = "hyperbolic";
	cout << function << endl;
	cout << "Ingrese el numero de capas ocultas: \n";
	cin >> NumberLayer;
	NumberLayer = NumberLayer + 2;
	int NumberNeuronsForLayer[NumberLayer];
	for (int i = 0; i < NumberLayer; i++)
	{
		if (i == 0)
			NumberNeuronsForLayer[i] = 11;
		else if (i == NumberLayer - 1)
			NumberNeuronsForLayer[i] = 1;
		else
		{
			NumberString << (i);
			cout << "Ingrese el numero de neuronas en la capa oculta numero N°" + NumberString.str() + "\n";
			NumberString.str("");
			cin >> NumberNeuronsForLayer[i];
		}
	}
	/*
	 * Creamos la arquitectura de la red segun los datos ingresados anteriormente
	 */
	 
	 neuron = new Neuron *[NumberLayer];
	 connection = new Connection **[NumberLayer];

	 NeuronError = new double *[NumberLayer];
	 for (int i = 0; i < NumberLayer; i++)
	 {
		 neuron[i] = new Neuron[NumberNeuronsForLayer[i]];
		 connection[i] = new Connection *[NumberNeuronsForLayer[i]];
		 NeuronError[i] = new double[NumberNeuronsForLayer[i]];
		 for (int j = 0; j < NumberNeuronsForLayer[i]; j++)
		 {
			 if (i < NumberLayer - 1)
				 connection[i][j] = new Connection[NumberNeuronsForLayer[i + 1]];
		 }
	 }
 
	 /*
	  * Realizamos el recorrido por cada una de las neuronas para establecer
	  * el numero de capa y el tipo de funcion en cada neurona y ademas
	  * inicializamos los pesos de las conexiones con valores aleatorios
	  */
	 for (int j = 0; j < NumberLayer; j++)
	 {
		 for (int k = 0; k < NumberNeuronsForLayer[j]; k++)
		 {
			 neuron[j][k].SetLayer(j);
			 string s = to_string(j) + to_string(k);
			 cout << s + "\n";
			 if (j == 0)
			 {
				 /*
				  * Seleccionamos el tipo de función en cada neurona
				  * además, indicamos que estas neuronas son de la capa de entrada
				  */
				 neuron[j][k].SetTypeFunction(TypeFunction);
				 neuron[j][k].SetIsInput(true);
			 }
			 else
			 {
				 /*
				  * También seleccionamos el tipo de función
				  * en este caso indicamos que estas neuronas no son de la capa de entrada
				  */
				 neuron[j][k].SetTypeFunction(TypeFunction);
				 neuron[j][k].SetIsInput(false);
				 random = ((double)rand() / (RAND_MAX));
				 neuron[j][k].SetBias(random);
			 }
 
			 /*
			  * Inicializamos los pesos de las conexiones con valores aletorios entre 0 - 1
			  */
			 if (j < NumberLayer - 1)
			 {
				 for (int g = 0; g < NumberNeuronsForLayer[j + 1]; g++)
				 {
					 random = ((double)rand() / (RAND_MAX));
					 connection[j][k][g].SetWeightValue(random);
					 cout << connection[j][k][g].GetWeightValue() << endl;
				 }
			 }
			 count++;
		 }
	 }
 
	 /*
	  * Llamamos a la clase DB para realizar la conexion a la base de datos y la consulta
	  * para obtener los datos de los patrones de entrenamiento
	  */
	 DB database;
	 bool resultDB;
 
	 resultDB = database.ConexionDB();
	 if (resultDB == true)
		 cout << "Open Database" << endl;
	 else
		 cout << "Error opening Database" << endl;
	 database.SetCount(0);
 
	 resultDB = database.GetStudents();
	 if (resultDB == true)
	 {
		 cout << "Student data obtained" << endl;
		 ArrayStudent = database.GetStudentsVector();
	 }
	 else
		 cout << "Error getting student data" << endl;
 
	 /*
	  * Acumulador para la sumatoria de la entrada efectiva de cada neurona
	  */
	 double auxmu = 0;
 
	 string textinsert = "";
 
	 /*
	  *	Recorremos los seis bloques de cros validation
	  */
	 double MSE = 0;
	 double validationError = 0;
	 double trainingTime; //OPENMP
 
	 LearningRatio = 0.00001;
 
	 CountLearningCycle = 1;
	 for (int j = 0; j < NumberLayer; j++)
	 {
		 for (int k = 0; k < NumberNeuronsForLayer[j]; k++)
		 {
 
			 if (j != 0)
			 {
				 /*
				  * También seleccionamos el tipo de función
				  * en este caso indicamos que estas neuronas no son de la capa de entrada
				  */
				 random = ((double)rand() / (RAND_MAX));
				 neuron[j][k].SetBias(random);
			 }
			 if (j < NumberLayer - 1)
			 {
				 for (int g = 0; g < NumberNeuronsForLayer[j + 1]; g++)
				 {
					 random = ((double)rand() / (RAND_MAX));
					 connection[j][k][g].SetWeightValue(random);
				 }
			 }
		 }
	 }
	 int totalNeurons=0;
	 for (int j = 0; j < NumberLayer; j++)
	 {
		 totalNeurons += NumberNeuronsForLayer[j];
	 }
	 Neuron * neuron1D;
	 Neuron * dev_neuron1D;
	 double * errorNeuron1D;
	 Connection * connection1D;
	 Connection * dev_connection1D;
	 double * dev_errorNeuron1D;
	 Student * dev_student;
	 double * dev_LearningRatio;
	 double * dev_error;
	 double * dev_auxmu;
	 int count=0;
	 int * dev_count;
	 n = 1;
	 int * dev_n;

	 neuron1D = (Neuron *)malloc(NumberLayer*totalNeurons*sizeof(Neuron));
	 connection1D = (Connection*)malloc(NumberLayer*totalNeurons*totalNeurons*sizeof(Connection));
	 errorNeuron1D = (double*)malloc(NumberLayer*totalNeurons*sizeof(double));
	 HANDLE_ERROR(cudaMalloc((void**)&dev_errorNeuron1D,NumberLayer*totalNeurons*sizeof(double)));
	 HANDLE_ERROR(cudaMalloc((void**)&dev_student,961*sizeof(Student)));
	 HANDLE_ERROR(cudaMalloc((void**)&dev_neuron1D,NumberLayer*totalNeurons*sizeof(Neuron)));
	 HANDLE_ERROR(cudaMalloc((void**)&dev_connection1D,NumberLayer*totalNeurons*totalNeurons*sizeof(Connection)));
	 HANDLE_ERROR(cudaMalloc((void**)&dev_n,sizeof(int)));
	 HANDLE_ERROR(cudaMalloc((void**)&dev_LearningRatio,sizeof(double)));
	 HANDLE_ERROR(cudaMalloc((void**)&dev_error,sizeof(double)));
	 HANDLE_ERROR(cudaMalloc((void**)&dev_count,sizeof(int)));
	 HANDLE_ERROR(cudaMalloc((void**)&dev_auxmu,totalNeurons*sizeof(double)));
	 int countNeuron = 0;
	 int * dev_countNeuron;
	 HANDLE_ERROR(cudaMalloc((void**)&dev_countNeuron,sizeof(int)));
	 int countConnection = 0;
	 for (int i = 0; i < NumberLayer; i++)
	 {																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																															
		 for (int j = 0; j < NumberNeuronsForLayer[i]; j++){
			 neuron1D[countNeuron] = neuron[i][j];
			 countNeuron++;
			 if(i < NumberLayer - 1){
				 for(int k=0;k<NumberNeuronsForLayer[i+1];k++){
					 connection1D[countConnection] = connection[i][j][k];
					 countConnection++;
				 }
			 }
		 }
	 }
	 clock_t begin = clock();
	HANDLE_ERROR(cudaMemcpy(dev_countNeuron,&countNeuron,sizeof(int),cudaMemcpyHostToDevice));
	HANDLE_ERROR(cudaMemcpy(dev_n,&n,sizeof(int),cudaMemcpyHostToDevice));
	HANDLE_ERROR(cudaMemcpy(dev_error,&error,sizeof(double),cudaMemcpyHostToDevice));
	HANDLE_ERROR(cudaMemcpy(dev_LearningRatio,&LearningRatio,sizeof(double),cudaMemcpyHostToDevice));
	HANDLE_ERROR(cudaMemcpy(dev_student,ArrayStudent,961*sizeof(Student),cudaMemcpyHostToDevice));
	HANDLE_ERROR(cudaMemcpy(dev_neuron1D,neuron1D,NumberLayer*totalNeurons*sizeof(Neuron*),cudaMemcpyHostToDevice));																																						
	HANDLE_ERROR(cudaMemcpy(dev_connection1D,connection1D,NumberLayer*totalNeurons*totalNeurons*sizeof(Connection),cudaMemcpyHostToDevice));
	HANDLE_ERROR(cudaMemcpy(dev_count,&count,sizeof(int),cudaMemcpyHostToDevice));
	while(CountLearningCycle <= LearningCycle){		
		_updateError<<<1,1>>>(dev_error);
		for(n = 1;n<800;n++){			
			readInput<<<1,11>>>(dev_neuron1D,dev_n,dev_student);
			forwardItoC<<<5,1024>>>(dev_neuron1D,dev_connection1D,dev_countNeuron,dev_auxmu);
			forwardCtoO<<<5,1024>>>(dev_neuron1D,dev_connection1D,dev_countNeuron,dev_auxmu,dev_count);
			getErrorOutput<<<1,1>>>(dev_neuron1D,dev_errorNeuron1D,dev_connection1D,dev_n,dev_countNeuron,dev_student);
			backpropagation<<<5,1024>>>(dev_neuron1D,dev_errorNeuron1D,dev_connection1D,dev_n,dev_countNeuron,dev_student);
			updateWeight_bias_input<<<5,1024>>>(dev_neuron1D,dev_errorNeuron1D,dev_connection1D,dev_countNeuron,dev_LearningRatio);
			updateWeight_bias_oculta<<<5,1024>>>(dev_neuron1D,dev_errorNeuron1D,dev_connection1D,dev_countNeuron,dev_LearningRatio);
			errorSum<<<1,1>>>(dev_neuron1D,dev_error,dev_student,dev_n,dev_countNeuron);
			updateN<<<1,1>>>(dev_n);
			//cout << "--------------" << endl;
		}
		_updateN<<<1,1>>>(dev_n);
		updateLearningRatio<<<1,1>>>(dev_LearningRatio);
		HANDLE_ERROR(cudaMemcpy(&error,dev_error,sizeof(double),cudaMemcpyDeviceToHost));

		trainingError = (1.0 / 800.0) * (0.5 * error);
		//cout << CountLearningCycle << endl;
		CountLearningCycle++;
	}
	cout << trainingError << endl;

	HANDLE_ERROR(cudaMemcpy(neuron1D,dev_neuron1D,NumberLayer*totalNeurons*sizeof(Neuron*),cudaMemcpyDeviceToHost));
	HANDLE_ERROR(cudaMemcpy(connection1D,dev_connection1D,NumberLayer*totalNeurons*totalNeurons*sizeof(Connection),cudaMemcpyDeviceToHost));

	cudaFree(dev_neuron1D);
	cudaFree(dev_connection1D);
	cudaFree(dev_student);
	cudaFree(dev_errorNeuron1D);
	cudaFree(dev_LearningRatio);
	cudaFree(dev_error);
	cudaFree(dev_n);
	cudaFree(dev_countNeuron);
    clock_t end = clock();
	double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	cout << "Training time is " << time_spent << endl;
	
	return 0;
}