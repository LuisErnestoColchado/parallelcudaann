#include <iostream>
#include <stdio.h>
#include <string>

using namespace std;

class Student{

public:

	string cod;
	string name;
	string age;
	double index1;
	double index2;
	double index3;
	double index4;
	double index5;
	double index6;
	double index7;
	double index8;
	double index9;
	double index10;
	double index11;
	double result;

	string cluster;
	
	string GetCod();
	void SetCod(string);

	string GetName();
	void SetName(string);

	string GetAge();
	void SetAge(string);

	double GetIndex1();
	void SetIndex1(double);

	double GetIndex2();
	void SetIndex2(double);

	double GetIndex3();
	void SetIndex3(double);

	double GetIndex4();
	void SetIndex4(double);

	double GetIndex5();
	void SetIndex5(double);

	double GetIndex6();
	void SetIndex6(double);

	double GetIndex7();
	void SetIndex7(double);

	double GetIndex8();
	void SetIndex8(double);

	double GetIndex9();
	void SetIndex9(double);

	double GetIndex10();
	void SetIndex10(double);

	double GetIndex11();
	void SetIndex11(double);

	double GetResult();
	void SetResult(double);

	string GetCluster();
	void SetCluster(string);


};

string Student::GetCod(){
	return cod;
}
void Student::SetCod(string pCod){
	cod = pCod;
}

string Student::GetName(){
	return name;
}
void Student::SetName(string pName){
	name = pName;
}

string Student::GetAge(){
	return age;
}
void Student::SetAge(string pAge){
	age = pAge;
}

double Student::GetIndex1(){
	return index1;
}
void Student::SetIndex1(double pIndex1){
	index1 = pIndex1;
}

double Student::GetIndex2(){
	return index2;
}
void Student::SetIndex2(double pIndex2){
	index2 = pIndex2;
}

double Student::GetIndex3(){
	return index3;
}
void Student::SetIndex3(double pIndex3){
	index3 = pIndex3;
}

double Student::GetIndex4(){
	return index4;
}
void Student::SetIndex4(double pIndex4){
	index4 = pIndex4;
}

double Student::GetIndex5(){
	return index5;
}
void Student::SetIndex5(double pIndex5){
	index5 = pIndex5;
}

double Student::GetIndex6(){
	return index6;
}
void Student::SetIndex6(double pIndex6){
	index6 = pIndex6;
}

double Student::GetIndex7(){
	return index6;
}
void Student::SetIndex7(double pIndex7){
	index7 = pIndex7;
}

double Student::GetIndex8(){
	return index8;
}
void Student::SetIndex8(double pIndex8){
	index8 = pIndex8;
}

double Student::GetIndex9(){
	return index9;
}
void Student::SetIndex9(double pIndex9){
	index9 = pIndex9;
}

double Student::GetIndex10(){
	return index10;
}
void Student::SetIndex10(double pIndex10){
	index10 = pIndex10;
}

double Student::GetIndex11(){
	return index11;
}
void Student::SetIndex11(double pIndex11){
	index11 = pIndex11;
}

double Student::GetResult(){
	return result;
}
void Student::SetResult(double pResult){
	result = pResult;
}

string Student::GetCluster(){
	return cluster;
}
void Student::SetCluster(string pCluster){
	cluster = pCluster;
}

